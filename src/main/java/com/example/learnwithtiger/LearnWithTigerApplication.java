package com.example.learnwithtiger;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LearnWithTigerApplication {

    public static void main(String[] args) {
        SpringApplication.run(LearnWithTigerApplication.class, args);
    }

}
